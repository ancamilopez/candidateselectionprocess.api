# Bienvenido  a API para la Gestión de Candidatos y Experiencias de Pandapé

Este API REST  ha sido diseñado y desarrollado para atender las necesidades de Pandapé, una empresa dedicada a la administración y selección de candidatos. El enfoque principal de este sistema se centra en la gestión integral de candidatos y sus experiencias laborales, con un énfasis especial en la implementación de principios SOLID y arquitecturas limpias.

En este proyecto, se ha implementado el patrón CQRS (Command Query Responsibility Segregation) para separar las operaciones de escritura y lectura, lo que mejora la escalabilidad y el rendimiento del sistema. Además, se ha aplicado el patrón Repository para la gestión de datos y el patrón Handler para el control de excepciones, lo que garantiza la robustez y la manejabilidad del sistema.

En resumen, este API proporciona una solución sólida y escalable para la gestión de candidatos y experiencias laborales, permitiendo a Pandapé optimizar sus operaciones de recursos humanos de manera efectiva y cumplir con los estándares más exigentes de desarrollo de software.



## Instalación

### Requerimientos

- Tener instalado SDK de .NET 6.0

### Clonar proyecto

Clone el proyecto en su maquina  por medio de la URl : *https://gitlab.com/ancamilopez/candidateselectionprocess.api.git* o si lo desea por medio de SSH

```sh
git clone https://gitlab.com/ancamilopez/candidateselectionprocess.api.git <NombreEnLocal> -b develop

```
Una vez clonado el proyecto Asegúrese de que las siguientes dependencias hayan sido cargadas correctamente:

```sh
Autofac.Extensions.DependencyInjection
MediatR
Microsoft.EntityFrameworkCore.InMemory
Newtonsoft.Json
```


Una vez cargadas las dependecias corectamente puede ejecutarlo si lo hace con  Visual Studio o si desea con el comando : 

```sh
dotnet run
```  
