﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using MediatR;

namespace CanditateSelectionProcessAPI.Infrastructure.Queries
{
    public record GetAllCandidateExperiencesByIdCandidateQuery(int Id) : IRequest<IEnumerable<CandidateWithExperienceResponseDto>>;
}
