﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using MediatR;

namespace CanditateSelectionProcessAPI.Infrastructure.Queries
{
    public record GetAllCandidatesQuery : IRequest<IEnumerable<CandidatesResponseDto>>;
    public record GetCandidateByIdQuery(int Id) : IRequest<CandidatesResponseDto>;
    public record GetCandidateByIdWithExperiencesQuery(int IdCandidate) : IRequest<CandidateWithExperienceResponseDto>;
}
