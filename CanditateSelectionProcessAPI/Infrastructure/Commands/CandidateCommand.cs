﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Domain;
using MediatR;

namespace CanditateSelectionProcessAPI.Infrastructure.Commands
{
    public record CreateCandidateCommand(CreateCandidateRequestDto candidate) : IRequest<CandidatesResponseDto>;
    public record CreateCandidateWithExperiencesCommand(CandidateWithExperienceRequestDto candidate) : IRequest<CandidatesResponseDto>;
    public record UpdateCandidateCommand(UpdateCandidatesRequestDto candidate) : IRequest<CandidatesResponseDto>;
    public record DeteleCandidateCommand(int Id) : IRequest<bool>;

}
