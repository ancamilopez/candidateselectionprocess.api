﻿using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.Domain;
using MediatR;

namespace CanditateSelectionProcessAPI.Infrastructure.Commands
{
    public class CandidateExperienceCommand
    {
        public record CreateCandidateExperienceCommand(CandidateExperiencesRequestDto expCandidate) : IRequest<CandidateExperienceResponseDto>;
        public record CreateCandidateExperienceListCommand(IEnumerable<CandidateExperiencesRequestDto> expCandidate) : IRequest<IEnumerable<CandidateExperienceResponseDto>>;
        public record UpdateCandidateExperienceCommand(UpdateCandidateExperiencesDto expCandidate) : IRequest<CandidateExperienceResponseDto>;
        public record DeleteCandidateExperienceCommand(int Id) : IRequest<bool>;
    }
}
