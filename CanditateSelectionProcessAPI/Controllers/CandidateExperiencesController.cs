﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateExperiencesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CandidateExperiencesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<CandidateExperienceResponseDto>> CreateCandidateExp(CreateCandidateExperienceCommand command)
        {
            var candidateItem = await _mediator.Send(command);
            return Ok(candidateItem);
        }

        [HttpPut("id")]
        public async Task<IActionResult> Update(int id, UpdateCandidateExperienceCommand command)
        {
            if (id != command.expCandidate.Id)
            {
                return BadRequest();
            }
            var candidateItem = await _mediator.Send(command);
            if (candidateItem == null)
            {
                return NotFound();
            }

            return Ok(candidateItem);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _mediator.Send(new DeleteCandidateExperienceCommand(id));
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
