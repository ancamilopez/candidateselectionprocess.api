﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using CanditateSelectionProcessAPI.Infrastructure.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CanditateSelectionProcessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidatesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CandidatesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CandidatesResponseDto>>> GetAll()
        {
            return Ok(await _mediator.Send(new GetAllCandidatesQuery()));
        }

        [HttpGet("id")]
        public async Task<ActionResult<CandidatesResponseDto>> GetById(int id)
        {
            var query = new GetCandidateByIdQuery(id);
            var candidateItem = await _mediator.Send(query);

            if (candidateItem == null)
            {
                return NotFound();
            }

            return Ok(candidateItem);
        }

        [HttpGet]
        [Route("GetCandidateWithExperiences/{id}")]
        public async Task<ActionResult<IEnumerable<CandidateWithExperienceResponseDto>>> GetCandidateWithExperien(int id)
        {
            var query = new GetCandidateByIdWithExperiencesQuery(id);
            var candidateExp= await _mediator.Send(query);

            if (candidateExp == null)
            {
                return NotFound();
            }

            return Ok(candidateExp);
        }

        [HttpPost]
        public async Task<ActionResult<CandidatesResponseDto>> CreateCandidate(CreateCandidateCommand command)
        {
            var candidateItem = await _mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { id = candidateItem.Id }, candidateItem);
        }

        [HttpPost]
        [Route("CreateCandidateWithExperiences")]
        public async Task<ActionResult<CandidateWithExperienceResponseDto>> CreateWithExperiences(CreateCandidateWithExperiencesCommand command)
        {
            var candidateItem = await _mediator.Send(command);
            return CreatedAtAction(nameof(GetCandidateWithExperien), new { id = candidateItem.Id }, candidateItem);
        }

        [HttpPut("id")]
        public async Task<IActionResult> Update(int id, UpdateCandidateCommand command)
        {
            if (id != command.candidate.Id)
            {
                return BadRequest();
            }

            var candidateItem = await _mediator.Send(command);
            if (candidateItem == null)
            {
                return NotFound();
            }

            return Ok(candidateItem);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _mediator.Send(new DeteleCandidateCommand(id));
            if (!result)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
