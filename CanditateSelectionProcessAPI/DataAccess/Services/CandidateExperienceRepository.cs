﻿using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CanditateSelectionProcessAPI.DataAccess.Services
{
    public class CandidateExperienceRepository : ICandidateExperienceRepository
    {

        public readonly ApplicationDbContext _dbContext;

        public CandidateExperienceRepository(ApplicationDbContext context)
        {
            _dbContext = context;
        }

        public async Task<IEnumerable<CandidateExperiences>> GetCandidateExperienceByIdCandidate(int Id)
        {
            return await _dbContext.CandidateExperiences.Where(_ => _.IdCandidate == Id).ToArrayAsync();
        }
    }
}
