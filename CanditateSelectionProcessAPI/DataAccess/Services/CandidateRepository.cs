﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace CanditateSelectionProcessAPI.DataAccess.Services
{
    public class CandidateRepository : ICandidateRepository
    {

        public readonly ApplicationDbContext _dbContext;

        public CandidateRepository(ApplicationDbContext context)
        {
            _dbContext = context;
        }


        public async Task<IEnumerable<Candidates>> GetAllCandidates()
        {
            return await _dbContext.Candidates.Where(_ => _.Status == true).ToListAsync();
        }
        public async Task<Candidates> GetCandidateById(int Id)
        {
            return await _dbContext.Candidates.FindAsync(new object[] { Id });
        }
    }
}
