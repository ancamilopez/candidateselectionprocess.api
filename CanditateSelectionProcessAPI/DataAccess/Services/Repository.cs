﻿using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CanditateSelectionProcessAPI.DataAccess.Services
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public readonly ApplicationDbContext _dbContext;

        public Repository(ApplicationDbContext context)
        {
            _dbContext = context;
        }

        protected DbSet<T> EntitySet
        {
            get
            {
                return _dbContext.Set<T>();
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await EntitySet.ToListAsync();
        }

        public async Task<T> GetByID(int? id)
        {
            return await EntitySet.FindAsync(id);
        }

        public async Task<T> Insert(T entity)
        {
            EntitySet.Add(entity);
            await Save();
            return entity;
        }

        public async Task<IEnumerable<T>> InsertList(IEnumerable<T> entity)
        {
            EntitySet.AddRange(entity);
            await Save();
            return entity;
        }

        public async Task<T> Delete(int id)
        {
            T entity = await EntitySet.FindAsync(id);
            EntitySet.Remove(entity);
            await Save();



            return entity;
        }

        public async Task Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await Save();
        }

        public async Task Save()
        {
            await _dbContext.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                _dbContext.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<T> Find(Expression<Func<T, bool>> expr)
        {
            return await EntitySet.AsNoTracking().FirstOrDefaultAsync(expr);
        }

    }
}
