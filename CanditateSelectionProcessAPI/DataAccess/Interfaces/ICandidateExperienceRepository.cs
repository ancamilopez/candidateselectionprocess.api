﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Domain;

namespace CanditateSelectionProcessAPI.DataAccess.Interfaces
{
    public interface ICandidateExperienceRepository
    {
        Task<IEnumerable<CandidateExperiences>> GetCandidateExperienceByIdCandidate(int Id);
    }
}
