﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Domain;

namespace CanditateSelectionProcessAPI.DataAccess.Interfaces
{
    public interface ICandidateRepository
    {
        Task<IEnumerable<Candidates>> GetAllCandidates();
        Task<Candidates> GetCandidateById(int Id);
    }
}
