﻿using System.ComponentModel.DataAnnotations;

namespace CanditateSelectionProcessAPI.Domain
{
    public class Candidates
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(150)]
        public string Surname { get; set; }
        [Required]
        public DateTime Birthdate { get; set; }
        [Required]
        [MaxLength(250)]
        public string Email { get; set; }
        [Required]
        public bool Status { get; set; }
        [Required]
        public DateTime InsertDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
