﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanditateSelectionProcessAPI.Domain
{
    public class CandidateExperiences
    {
        public int Id { get; set; }
        [ForeignKey("IdCandidateFK")]
        public int IdCandidate { get; set; }
        [Required]
        [MaxLength(100)]
        public string Company { get; set; }
        [Required]
        [MaxLength(100)]
        public string Job { get; set; }
        [Required]
        [MaxLength(4000)]
        public string Description { get; set; }
        [Required]
        public decimal Salary { get; set; }
        [Required]
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public bool Status { get; set; }
        [Required]
        public DateTime InsertDate { get; set; }
        public DateTime ModifyDate { get; set; }

    }
}
