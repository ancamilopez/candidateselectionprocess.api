﻿using CanditateSelectionProcessAPI.Domain;
using Microsoft.EntityFrameworkCore;

namespace CanditateSelectionProcessAPI.Domain.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Candidates> Candidates { get; set; }
        public DbSet<CandidateExperiences> CandidateExperiences { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

    }
}
