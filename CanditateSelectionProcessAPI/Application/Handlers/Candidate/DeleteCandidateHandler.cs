﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{

    public class DeleteCandidateHandler : IRequestHandler<DeteleCandidateCommand, bool>
    {
        public readonly IRepository<Candidates> _IRepository;

        public DeleteCandidateHandler(IRepository<Candidates> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<bool> Handle(DeteleCandidateCommand request, CancellationToken cancellationToken)
        {
            var candidateItem = await _IRepository.Find(f => f.Id == request.Id);

            if (candidateItem == null)
            {
                return false;
            }

            candidateItem.Status = false;
            await _IRepository.Update(candidateItem);

            return true;
        }
    }
}
