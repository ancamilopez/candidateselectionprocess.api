﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class UpdateCandidateHandler : IRequestHandler<UpdateCandidateCommand, CandidatesResponseDto>
    {
        public readonly IRepository<Candidates> _IRepository;

        public UpdateCandidateHandler(IRepository<Candidates> iRepository)
        {
            _IRepository = iRepository;
        }
        public async Task<CandidatesResponseDto> Handle(UpdateCandidateCommand request, CancellationToken cancellationToken)
        {
            var candidateItem = await _IRepository.Find(f => f.Id == request.candidate.Id);

            if (candidateItem == null)
            {
                return null; 
            }

            candidateItem.Name = request.candidate.Name;
            candidateItem.Surname = request.candidate.Surname;
            candidateItem.Birthdate = request.candidate.Birthdate;
            candidateItem.Email = request.candidate.Email;
            candidateItem.ModifyDate = DateTime.Now;

            await _IRepository.Update(candidateItem);

            return new CandidatesResponseDto
            {
                Id = candidateItem.Id,
                Name = candidateItem.Name,
                Surname = candidateItem.Surname,
                Birthdate = candidateItem.Birthdate,
                Email = candidateItem.Email,
                InsertDate = candidateItem.InsertDate,
                ModifyDate = candidateItem.ModifyDate,
            };
        }
    }
}
