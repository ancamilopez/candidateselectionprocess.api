﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class CreateCandidateHandler : IRequestHandler<CreateCandidateCommand, CandidatesResponseDto>
    {
        public readonly IRepository<Candidates> _IRepository;

        public CreateCandidateHandler(IRepository<Candidates> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<CandidatesResponseDto> Handle(CreateCandidateCommand request, CancellationToken cancellationToken)
        {
            Candidates objCandidate = new Candidates
            {
                Name = request.candidate.Name,
                Surname = request.candidate.Surname,
                Birthdate = request.candidate.Birthdate,
                Email = request.candidate.Email,
                InsertDate = DateTime.Now,
                Status = true
            }; 

            await _IRepository.Insert(objCandidate);

            return new CandidatesResponseDto
            {
                Id = objCandidate.Id,
                Name = objCandidate.Name,
                Surname = objCandidate.Surname,
                Birthdate = objCandidate.Birthdate,
                Email = objCandidate.Email,
                InsertDate = objCandidate.InsertDate,
                Status = objCandidate.Status
            };
        }
    }
}
