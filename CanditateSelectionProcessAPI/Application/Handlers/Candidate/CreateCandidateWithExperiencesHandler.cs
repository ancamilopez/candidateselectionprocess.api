﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class CreateCandidateWithExperiencesHandler : IRequestHandler<CreateCandidateWithExperiencesCommand, CandidatesResponseDto>
    {
        public readonly IRepository<Candidates> _IRepository;
        private readonly IMediator _mediator;

        public CreateCandidateWithExperiencesHandler(IRepository<Candidates> iRepository, IMediator mediator)
        {
            _IRepository = iRepository;
            _mediator = mediator;
        }

        public async Task<CandidatesResponseDto> Handle(CreateCandidateWithExperiencesCommand request, CancellationToken cancellationToken)
        {
            Candidates objCandidate = new Candidates
            {
                Name = request.candidate.Name,
                Surname = request.candidate.Surname,
                Birthdate = request.candidate.Birthdate,
                Email = request.candidate.Email,
                InsertDate = DateTime.Now,
                Status = true
            };

            await _IRepository.Insert(objCandidate);

            foreach (var i in request.candidate.CandidateExperiencesRequestDto)
            {
                i.IdCandidate = objCandidate.Id;
            }

            var responseExp = await _mediator.Send(new CreateCandidateExperienceListCommand(request.candidate.CandidateExperiencesRequestDto), cancellationToken);

            return new CandidatesResponseDto
            {
                Id = objCandidate.Id,
                Name = objCandidate.Name,
                Surname = objCandidate.Surname,
                Birthdate = objCandidate.Birthdate,
                Email = objCandidate.Email,
                InsertDate = objCandidate.InsertDate,
                Status = objCandidate.Status
            };
        }
    }
}
}
