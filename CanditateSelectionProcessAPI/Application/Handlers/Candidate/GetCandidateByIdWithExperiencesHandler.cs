﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class GetCandidateByIdWithExperiencesHandler : IRequestHandler<GetCandidateByIdWithExperiencesQuery, CandidateWithExperienceResponseDto>
    {
        public readonly ICandidateRepository _ICandidateRepository;
        public readonly ICandidateExperienceRepository _ICandidateExperienceRepository;
        public GetCandidateByIdWithExperiencesHandler(ICandidateRepository iCandidateRepository, ICandidateExperienceRepository iCandidateExperienceRepository)
        {
            _ICandidateRepository = iCandidateRepository;
            _ICandidateExperienceRepository = iCandidateExperienceRepository;
        }

        public async Task<CandidateWithExperienceResponseDto> Handle(GetCandidateByIdWithExperiencesQuery request, CancellationToken cancellationToken)
        {
            var candidateItem = await _ICandidateRepository.GetCandidateById(request.IdCandidate);
            if (candidateItem == null)
            {
                return null;
            }

            var resultCandidateExperiences = await _ICandidateExperienceRepository.GetCandidateExperienceByIdCandidate(request.IdCandidate);

            var candidateExperiencesList = resultCandidateExperiences.Where(_ => _.IdCandidate == request.IdCandidate).Select(i => new CandidateExperienceResponseDto
            {
                Id = i.Id,
                IdCandidate = i.IdCandidate,
                Company = i.Company,
                Job = i.Job,
                Description = i.Description,
                BeginDate = i.BeginDate,
                EndDate = i.EndDate,
                Salary = i.Salary
            });



            List<CandidateExperienceResponseDto> listExperiences = new List<CandidateExperienceResponseDto>();
            foreach (var i in candidateExperiencesList)
            {
                CandidateExperienceResponseDto objExp = new CandidateExperienceResponseDto()
                {
                    Id = i.Id,
                    IdCandidate = i.IdCandidate,
                    Company = i.Company,
                    Job = i.Job,
                    Description = i.Description,
                    BeginDate = i.BeginDate,
                    EndDate = i.EndDate,
                    Salary = i.Salary
                };
                listExperiences.Add(objExp);
            }

            return new CandidateWithExperienceResponseDto
            {
                Id = candidateItem.Id,
                Name = candidateItem.Name,
                Surname = candidateItem.Surname,
                Birthdate = candidateItem.Birthdate,
                Email = candidateItem.Email,
                InsertDate = candidateItem.InsertDate,
                CandidateExperiencesDto = listExperiences,
                ModifyDate = candidateItem.ModifyDate
            };
        }
    }
}
