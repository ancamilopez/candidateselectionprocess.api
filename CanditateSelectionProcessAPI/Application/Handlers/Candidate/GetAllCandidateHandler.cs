﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class GetAllCandidateHandler : IRequestHandler<GetAllCandidatesQuery, IEnumerable<CandidatesResponseDto>>
    {
        public readonly ICandidateRepository _ICandidateRepository;
        public GetAllCandidateHandler(ICandidateRepository iCandidateRepository)
        {
            _ICandidateRepository = iCandidateRepository;
        }

        public async Task<IEnumerable<CandidatesResponseDto>> Handle(GetAllCandidatesQuery request, CancellationToken cancellationToken)
        {
            var candidatesList = await _ICandidateRepository.GetAllCandidates();

            return candidatesList.Select(candidate => new CandidatesResponseDto
            {
                Id = candidate.Id,
                Name = candidate.Name,
                Surname = candidate.Surname,
                Birthdate = candidate.Birthdate,
                Email = candidate.Email,
                Status = candidate.Status,
                InsertDate = candidate.InsertDate,
                ModifyDate = candidate.ModifyDate
            });
        }
    }
}
