﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CanditateSelectionProcessAPI.Application.Handlers.Candidate
{
    public class GetByIdCandidateHandler : IRequestHandler<GetCandidateByIdQuery, CandidatesResponseDto>
    {
        public readonly ICandidateRepository _ICandidateRepository;

        public GetByIdCandidateHandler(ICandidateRepository iCandidateRepository)
        {
            _ICandidateRepository = iCandidateRepository;
        }

        public async Task<CandidatesResponseDto> Handle(GetCandidateByIdQuery request, CancellationToken cancellationToken)
        {
            var candidateItem = await _ICandidateRepository.GetCandidateById(request.Id);
            if (candidateItem == null)
            {
                return null;
            }

            return  new CandidatesResponseDto
            {
                Id = candidateItem.Id,
                Name = candidateItem.Name,
                Surname = candidateItem.Surname,
                Birthdate = candidateItem.Birthdate,
                Email = candidateItem.Email,
                Status = candidateItem.Status,
                InsertDate = candidateItem.InsertDate,
                ModifyDate = candidateItem.ModifyDate
            };
        }
    }
}
