﻿using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Infrastructure.Queries;
using MediatR;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Application.Handlers.CandidateExperience
{
    public class CreateCandidateExperienceListHandler : IRequestHandler<CreateCandidateExperienceListCommand, IEnumerable<CandidateExperienceResponseDto>>
    {
        public readonly IRepository<CandidateExperiences> _IRepository;

        public CreateCandidateExperienceListHandler(IRepository<CandidateExperiences> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<IEnumerable<CandidateExperienceResponseDto>> Handle(CreateCandidateExperienceListCommand request, CancellationToken cancellationToken)
        {
            List<CandidateExperienceResponseDto> listReturn = new List<CandidateExperienceResponseDto>();
            List<CandidateExperiences> modelExperiences = request.expCandidate.Select(dto => new CandidateExperiences
            {
                IdCandidate = dto.IdCandidate,
                Company = dto.Company,
                Job = dto.Job,
                Description = dto.Description,
                Salary = dto.Salary,
                BeginDate = dto.BeginDate,
                EndDate = dto.EndDate,
                Status = true,
                InsertDate = DateTime.Now,
            }).ToList();

            var result = await _IRepository.InsertList(modelExperiences);

            foreach (var i in result)
            {
                CandidateExperienceResponseDto objCanExp = new CandidateExperienceResponseDto()
                {
                    Id = i.Id,
                    IdCandidate = i.IdCandidate,
                    Company = i.Company,
                    Job = i.Job,
                    Description = i.Description,
                    BeginDate = i.BeginDate,
                    EndDate = i.EndDate,
                    Salary = i.Salary
                };
                listReturn.Add(objCanExp);
            }

            return listReturn;
        }
    }
}
