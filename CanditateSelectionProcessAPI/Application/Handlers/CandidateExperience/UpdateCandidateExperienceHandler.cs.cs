﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Application.Handlers.CandidateExperience
{
    public class UpdateCandidateExperienceHandler : IRequestHandler<UpdateCandidateExperienceCommand, CandidateExperienceResponseDto>
    {
        public readonly IRepository<CandidateExperiences> _IRepository;

        public UpdateCandidateExperienceHandler(IRepository<CandidateExperiences> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<CandidateExperienceResponseDto> Handle(UpdateCandidateExperienceCommand request, CancellationToken cancellationToken)
        {
            var expItem = await _IRepository.Find(f => f.Id == request.expCandidate.Id);

            if (expItem == null)
            {
                return null;
            }

            expItem.Company = request.expCandidate.Company;
            expItem.Job = request.expCandidate.Job;
            expItem.Description = request.expCandidate.Description;
            expItem.Salary = request.expCandidate.Salary;
            expItem.BeginDate = request.expCandidate.BeginDate;
            expItem.EndDate = request.expCandidate.EndDate;
            expItem.ModifyDate = DateTime.Now;

            await _IRepository.Update(expItem);

            return new CandidateExperienceResponseDto
            {
                Id = expItem.Id,
                IdCandidate = expItem.IdCandidate,
                Company = expItem.Company,
                Job = expItem.Job,
                Description = expItem.Description,
                Salary = expItem.Salary,
                BeginDate = expItem.BeginDate,
                EndDate = expItem.EndDate,
                ModifyDate = expItem.ModifyDate
            };
        }
    }
}
