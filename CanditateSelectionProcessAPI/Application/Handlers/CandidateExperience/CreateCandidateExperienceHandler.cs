﻿using CanditateSelectionProcessAPI.Application.DTOs.Candidate;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;
using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.DataAccess.Services;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Application.Handlers.CandidateExperience
{
    public class CreateCandidateExperienceHandler : IRequestHandler<CreateCandidateExperienceCommand, CandidateExperienceResponseDto>
    {
        public readonly IRepository<CandidateExperiences> _IRepository;

        public CreateCandidateExperienceHandler(IRepository<CandidateExperiences> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<CandidateExperienceResponseDto> Handle(CreateCandidateExperienceCommand request, CancellationToken cancellationToken)
        {
            CandidateExperiences objCandidateExp = new CandidateExperiences
            {
                IdCandidate = request.expCandidate.IdCandidate,
                Company = request.expCandidate.Company,
                Job = request.expCandidate.Job,
                Description = request.expCandidate.Description,
                Salary = request.expCandidate.Salary,
                BeginDate = request.expCandidate.BeginDate,
                EndDate = request.expCandidate.EndDate,
                InsertDate = DateTime.Now,
                Status = true
            };

            await _IRepository.Insert(objCandidateExp);

            return new CandidateExperienceResponseDto
            {
                Id = objCandidateExp.IdCandidate,
                IdCandidate = objCandidateExp.IdCandidate,
                Company = objCandidateExp.Company,
                Job = objCandidateExp.Job,
                Description = objCandidateExp.Description,
                Salary = objCandidateExp.Salary,
                BeginDate = objCandidateExp.BeginDate,
                EndDate = objCandidateExp.EndDate,
            };
        }
    }
}
