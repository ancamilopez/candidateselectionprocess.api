﻿using CanditateSelectionProcessAPI.DataAccess.Interfaces;
using CanditateSelectionProcessAPI.Domain;
using CanditateSelectionProcessAPI.Domain.Data;
using CanditateSelectionProcessAPI.Infrastructure.Commands;
using MediatR;
using static CanditateSelectionProcessAPI.Infrastructure.Commands.CandidateExperienceCommand;

namespace CanditateSelectionProcessAPI.Application.Handlers.CandidateExperience
{
    public class DeleteCandidateExperienceHandler : IRequestHandler<DeleteCandidateExperienceCommand, bool>
    {
        public readonly IRepository<CandidateExperiences> _IRepository;

        public DeleteCandidateExperienceHandler(IRepository<CandidateExperiences> iRepository)
        {
            _IRepository = iRepository;
        }

        public async Task<bool> Handle(DeleteCandidateExperienceCommand request, CancellationToken cancellationToken)
        {
            var experienceItem = await _IRepository.Find(f => f.Id == request.Id);

            if (experienceItem == null)
            {
                return false;
            }

            experienceItem.Status = false;

            await _IRepository.Update(experienceItem);

            return true;
        }
    }
}
