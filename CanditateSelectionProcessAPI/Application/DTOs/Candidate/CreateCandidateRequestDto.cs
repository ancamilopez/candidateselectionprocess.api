﻿using CanditateSelectionProcessAPI.Domain;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanditateSelectionProcessAPI.Application.DTOs.Candidate
{
    public class CreateCandidateRequestDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        public string Email { get; set; }
    }

}
