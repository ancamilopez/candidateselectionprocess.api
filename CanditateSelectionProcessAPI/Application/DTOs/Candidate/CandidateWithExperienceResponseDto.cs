﻿using CanditateSelectionProcessAPI.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using CanditateSelectionProcessAPI.Application.DTOs.CandidateExperience;

namespace CanditateSelectionProcessAPI.Application.DTOs.Candidate
{
    public class CandidateWithExperienceResponseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        public string Email { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public List<CandidateExperienceResponseDto> CandidateExperiencesDto { get; set; }
    }
}
